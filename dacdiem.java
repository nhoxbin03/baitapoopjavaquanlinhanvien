package danhsachquanlinhanvien;

public class dacdiem {
public String name;
public int age;
public String gender;
public String address;

public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}

public dacdiem (String name, int age, String gender, String address) {
	this.setName(name);
	this.setAge(age);
	this.setGender(gender);
	this.setAddress(address);
}

 

public void showInfo() {
	 System.out.println("dacdiem info:" );
	 System.out.println("-Name\t\t :" + this.getName());
	 System.out.println("-Age\t\t: " + this.getAge());
	 System.out.println("-Gender\t\t: " + this.getGender());
	 System.out.println("-Address\t\t: " + this.getAddress());
 }
	 
 }

